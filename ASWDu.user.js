// ==UserScript==
// @name         Another Steam Workshop Download userscript (ASWDu)
// @namespace    https://gitlab.com/timescam/ASWDu
// @version      0.4
// @description  Another Steam Workshop Download userscript, adds a button to the Steam Workshop pages that lets you head straight to the specific addon page at steamworkshopdownloader.io
// @author       timescam
// @include      *steamcommunity.com/sharedfiles/filedetails/?id=*
// ==/UserScript==

var url =
	"https://steamworkshopdownloader.io/download/" +
	new URLSearchParams(window.location.search).get("id");
var realButton = document.getElementById("SubscribeItemBtn");

realButton.parentElement.innerHTML +=
	"<a onclick=\"window.open('" +
	url +
	'\', \'_blank\').focus();" id="DownloadItemBtn" class="btn_green_white_innerfade btn_border_2px btn_medium"><span style="padding-right:32px" class="subscribeText">Download</span></a>';
