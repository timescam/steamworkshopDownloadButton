# ASWDu

Project now archived, due to [steamworkshopdownloader.io](https://steamworkshopdownloader.io) no longer serving files through their network ([Statement](https://www.reddit.com/r/swd_io/comments/uy55qg/we_are_no_longer_serving_any_files_through_our/)).
You can use [steamworkshop.download](http://steamworkshop.download/) instead

~~A Steam Workshop Download userscript powered by [steamworkshopdownloader.io](https://steamworkshopdownloader.io)
the script was tested on Edge 98.0.1089.1 dev with Tampermonkey BETA 4.14.6148~~




## Getting started

### Step 1: install a user script manager

To use user scripts you need to first install a user script manager. Which user script manager you can use depends on which browser you use.

- Chrome: [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) or [Violentmonkey](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag)

- Firefox: [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/), [Tampermonkey](https://addons.mozilla.org/firefox/addon/tampermonkey/), or [Violentmonkey](https://addons.mozilla.org/firefox/addon/violentmonkey/)

### Step 2: install the user script

[Click here to install](https://gitlab.com/timescam/ASWDu/-/raw/main/ASWDu.user.js)

### Step 3: profit?

![screenshot](screenshot.png)


## License

[The Unlicense](LICENSE)



## Credits

[steamworkshopdownloader.io](https://steamworkshopdownloader.io) for the download backend

[http://st.abcvg.info/swd/steamwd.user.js](http://st.abcvg.info/swd/steamwd.user.js) for the inspiration
